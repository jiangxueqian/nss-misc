/*
 * search.c
 *
 * restart the query with search domain appended.
 */
#define _DEFAULT_SOURCE
#define _POSIX_C_SOURCE 200809L
#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <netdb.h>
#include <nss.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#ifndef MODNAME
# define MODNAME search
#endif
#define STRINGIFY_(x) #x
#define STRINGIFY(x) STRINGIFY_(x)
#define CONCAT3_(a, b, c) a##b##c
#define CONCAT3(a, b, c) CONCAT3_(a, b, c)
#define MODNAME_S STRINGIFY(MODNAME)
#define _nss_search_gethostbyname2_r CONCAT3(_nss_, MODNAME, _gethostbyname2_r)
#define _nss_search_gethostbyname_r CONCAT3(_nss_, MODNAME, _gethostbyname_r)

static const char * const conf_files[] = {
	"/etc/nss-" MODNAME_S ".conf",
	"/usr/local/etc/nss-" MODNAME_S ".conf",
	"/etc/resolv.conf",
	NULL
};

static FILE *open_conf_file(void)
{
	const char * const *p = conf_files;
	FILE *r;
	for (; *p != NULL; p++)
		if ((r = fopen(*p, "r")) != NULL)
			return r;
	return NULL;
}

struct search_ent {
	struct search_ent *next;
	char *name;
};

struct search_ctx {
	struct search_ent *c;
	struct search_ent *n;
	int ndots;
};

static void search_ctx_append(struct search_ctx *c, const char *n)
{
	struct search_ent *a = malloc(sizeof(*a));
	if (a == NULL)
		return;
	if ((a->name = strdup(n)) == NULL) {
		free(a);
		return;
	}
	a->next = c->n;
	c->n = a;
}

static char *rtrim(char *c)
{
	size_t l = strlen(c);
	char *p = c + l;
	while (p > c) {
		if (isspace(*p))
			*p = 0;
		p--;
	}
	return c;
}

static void search_ctx_append_file(struct search_ctx *c, FILE *f, char **domain)
{
	char buf[1000];
	while (fgets(buf, 1000, f)) {
		char *saveptr;
		char *cmd = strtok_r(buf, " \t", &saveptr);
		if (cmd) {
			if (!strcmp(cmd, "search")) {
				char *p = strtok_r(NULL, " \t", &saveptr);
				for (; p != NULL; p = strtok_r(NULL, " \t",
							&saveptr))
					search_ctx_append(c, rtrim(p));
			} else if (!strcmp(cmd, "domain")) {
				if (*domain)
					free(*domain);
				*domain = strdup(rtrim(strtok_r(NULL, " \t",
							&saveptr)));
			} else if (!strcmp(cmd, "include")) {
				char *p = strtok_r(NULL, " \t", &saveptr);
				for (; p != NULL; p = strtok_r(NULL, " \t",
							&saveptr)) {
					FILE *i = fopen(p, "r");
					if (i) {
						search_ctx_append_file(c, i,
								domain);
						fclose(i);
					}
				}
			} else if (!strcmp(cmd, "options")) {
				char *p = strtok_r(NULL, " \t", &saveptr);
				for (; p != NULL; p = strtok_r(NULL, " \t",
							&saveptr))
					if (strlen(p) >= 7 &&
							!memcmp(p, "ndots:", 6))
						c->ndots = atoi(p + 6);
			}
		}
	}
}

static char *getdomain(void)
{
	char buf[HOST_NAME_MAX + 1];
	if (gethostname(buf, HOST_NAME_MAX + 1))
		return NULL;
	char *dot = strchr(buf, '.');
	if (dot == NULL)
		return NULL;
	char *dom = dot + 1;
	rtrim(dom);
	if (*dom == 0 || !strcmp(dom, "."))
		return NULL;
	return strdup(dom);
}

static struct search_ctx *search_begin(FILE *conf_file)
{
	struct search_ctx *r = malloc(sizeof(*r));
	if (r == NULL)
		return NULL;
	r->n = NULL;
	r->ndots = 1;
	char *domain = NULL;
	search_ctx_append_file(r, conf_file, &domain);
	if (r->n == NULL) {
		if (domain == NULL)
			domain = getdomain();
		if (domain == NULL)
			return r;
		struct search_ent *e = malloc(sizeof(*e));
		if (e == NULL)
			return r;
		e->next = NULL;
		e->name = domain;
		r->n = e;
		domain = NULL;
	}
	if (domain)
		free(domain);
	struct search_ent *p = NULL;
	while (r->n) {
		struct search_ent *t = r->n;
		r->n = t->next;
		t->next = p;
		p = t;
	}
	r->n = p;
	return r;
}

static int search_ndots(struct search_ctx *ctx)
{
	return ctx->ndots;
}

static void search_reset(struct search_ctx *ctx)
{
	ctx->c = ctx->n;
}

static const char *search_next(struct search_ctx *ctx)
{
	if (ctx->c == NULL)
		return NULL;
	const char *r = ctx->c->name;
	ctx->c = ctx->c->next;
	return r;
}

static void search_end(struct search_ctx *ctx)
{
	struct search_ent *p, *n;
	for (p = ctx->n; p != NULL; p = n) {
		free(p->name);
		n = p->next;
		free(p);
	}
	free(ctx);
}

static int is_name_searched(const char *name, struct search_ctx *c)
{
	search_reset(c);
	const char *s;
	for (s = search_next(c); s != NULL; s = search_next(c)) {
		size_t l = strlen(s);
		char psn[l + 2];
		if (s[0] == '.') {
			psn[0] = 0;
		} else {
			psn[0] = '.';
			psn[1] = 0;
			l++;
		}
		strcat(psn, s);
		size_t ln = strlen(name);
		if (ln < l)
			continue;
		if (!strcmp(name + (ln - l), psn))
			return 1;
	}
	return 0;
}

static enum nss_status
query(const char *name, int af,
		struct hostent *ret, char *buf, size_t buflen,
		int *errnop, int *h_errnop)
{
	struct hostent *result;
	gethostbyname2_r(name, af, ret, buf, buflen, &result, h_errnop);
	*errnop = errno;
	if (result == NULL)
		return NSS_STATUS_NOTFOUND;
	return NSS_STATUS_SUCCESS;
}

static unsigned countchr(const char *s, int c)
{
	unsigned r = 0;
	for (; *s != 0; s++)
		if (*s == c)
			r++;
	return r;
}

extern enum nss_status
_nss_search_gethostbyname2_r(const char *name, int af,
		struct hostent *ret, char *buf, size_t buflen,
		int *errnop, int *h_errnop)
{
	size_t nl = strlen(name);
	if (nl == 0 || name[nl - 1] == '.')
		return NSS_STATUS_NOTFOUND;

	FILE *conf = open_conf_file();
	if (!conf)
		return NSS_STATUS_UNAVAIL;
	struct search_ctx *c = search_begin(conf);
	fclose(conf);
	if (!c)
		return NSS_STATUS_UNAVAIL;
	enum nss_status retu = NSS_STATUS_NOTFOUND;
	if (countchr(name, '.') >= search_ndots(c))
		goto exit;
	if (is_name_searched(name, c))
		goto exit;

	search_reset(c);
	const char *sn;
	for (sn = search_next(c); sn != NULL; sn = search_next(c)) {
		char nn[nl + strlen(sn) + 2];
		strcpy(nn, name);
		if (sn[0] != '.')
			strcat(nn, ".");
		strcat(nn, sn);
		if (query(nn, af, ret, buf, buflen, errnop, h_errnop) ==
				NSS_STATUS_SUCCESS) {
			retu = NSS_STATUS_SUCCESS;
			goto exit;
		}
	}

exit:
	search_end(c);
	return retu;
}

extern enum nss_status
_nss_search_gethostbyname_r(const char *name,
		struct hostent *ret, char *buf, size_t buflen,
		int *errnop, int *h_errnop)
{
	return _nss_search_gethostbyname2_r(name, AF_UNSPEC,
			ret, buf, buflen, errnop, h_errnop);
}
