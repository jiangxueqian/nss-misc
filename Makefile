MODULES := $(patsubst %.c,%,$(wildcard *.c))
SOVER    = 2
TARGETS := $(addsuffix .so.$(SOVER),$(addprefix libnss_,$(MODULES)))

ifdef DEBUG
ECFLAGS += -Og -ggdb
endif

CFLAGS  += -Wall -std=c11 -Os -fPIC $(ECFLAGS)
LDFLAGS += -shared

all: $(TARGETS)

libnss_%.so.$(SOVER): %.c
	$(CC) -o $@ $(CFLAGS) $(LDFLAGS) -DMODNAME=$(patsubst libnss_%.so.$(SOVER),%,$@) -Wl,-soname,$@ $<

clean:
	-rm -f $(TARGETS)

.PHONY: all clean
