/*
 * blacklist.c
 *
 * blacklist resolver. Reads regexes from config files and match them agains the
 * query name. If any regexes successfully matches, returns notfound, unavail
 * otherwise.
 *
 * append [notfound=return] after module name in nsswitch.conf
 */
#include <netdb.h>
#include <nss.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef MODNAME
# define MODNAME blacklist
#endif
#define STRINGIFY_(x) #x
#define STRINGIFY(x) STRINGIFY_(x)
#define CONCAT3_(a, b, c) a##b##c
#define CONCAT3(a, b, c) CONCAT3_(a, b, c)
#define MODNAME_S STRINGIFY(MODNAME)
#define _nss_blacklist_gethostbyname2_r CONCAT3(_nss_, MODNAME, _gethostbyname2_r)
#define _nss_blacklist_gethostbyname_r CONCAT3(_nss_, MODNAME, _gethostbyname_r)

static const char * const regex_list_files[] = {
	"/etc/nss-" MODNAME_S ".conf",
	"/usr/local/etc/nss-" MODNAME_S ".conf",
	NULL
};

static FILE *open_regex_list(void)
{
	const char * const *p = regex_list_files;
	FILE *r;
	for (; *p != NULL; p++)
		if ((r = fopen(*p, "r")) != NULL)
			return r;
	return NULL;
}

static int is_name_blacklisted(const char *name)
{
	FILE *f = open_regex_list();
	if (f == NULL)
		return 2;

	int ret = 0;

	char regline[4096];
	while (fgets(regline, 4096, f) != NULL) {
		size_t l = strlen(regline);
		if (l != 0)
			regline[l - 1] = 0;
		regex_t re;
		if (regcomp(&re, regline, REG_EXTENDED | REG_ICASE))
			continue;
		if (regexec(&re, name, 0, NULL, 0) != REG_NOMATCH)
			ret = 1;
		regfree(&re);
		if (ret != 0)
			break;
	}

	fclose(f);
	return ret;
}

extern enum nss_status
_nss_blacklist_gethostbyname2_r(const char *name, int af,
		struct hostent *ret, char *buf, size_t buflen,
		int *errnop, int *h_errnop)
{
	if (is_name_blacklisted(name) == 1)
		return NSS_STATUS_NOTFOUND;
	return NSS_STATUS_UNAVAIL;
}

extern enum nss_status
_nss_blacklist_gethostbyname_r(const char *name,
		struct hostent *ret, char *buf, size_t buflen,
		int *errnop, int *h_errnop)
{
	return _nss_blacklist_gethostbyname2_r(name, AF_UNSPEC,
			ret, buf, buflen, errnop, h_errnop);
}
